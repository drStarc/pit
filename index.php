<?php
header("Content-type: text/html; charset=utf-8");
include_once $_SERVER['DOCUMENT_ROOT']."/function/includ.php";
$connect = startup();
if(isset($_POST['other_date'])){
$date = $_POST['other_date'];  
} else {
    $date = date('Y-m-d');
}
$query_prot = "SELECT prot FROM bgu WHERE date = '$date'";
$prot = all($connect, $query_prot);

$query_cals = "SELECT cals FROM bgu WHERE date = '$date'";
$cals = all($connect, $query_cals);
$message = add($connect,$prot, $cals);

$query_date_min = "SELECT MIN(date) FROM bgu";
$date_min = all($connect, $query_date_min);
// Информация для отображения
$title_page = 'Главная';

// Внутренний шаблон.
$content_page = view_include('themes/v_index.php',
        array('cals'=>$cals, 'prot'=>$prot, 'message'=>$message, 'date'=>$date));

// Внешний шаблон.
$page = view_include('themes/v_main.php', 
    array('title_page'=>$title_page, 'content_page'=>$content_page, 'date_min'=>$date_min));

echo $page;