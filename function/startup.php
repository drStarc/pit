<?php
function startup() {
    setlocale(LC_ALL, "ru_RU.UTF-8");
    mb_internal_encoding('UTF-8');
    $database_host='localhost';
    $database_user='userpit';
    $database_password='passpit';
    $database_name='pit';
    $connect = mysqli_connect($database_host, $database_user, $database_password, $database_name) or die("Error");
    mysqli_query($connect, "SET NAMES utf8");
    session_start();
    return $connect;
}