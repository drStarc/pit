<?php
$proc=$prot*100/200;
$no_proc=100-$proc;
$calsc=$cals*100/1925;
$no_cals=100-$calsc;
?>
<br>
<div><?echo $date?></div><br>
<form method='post' enctype='multipart/form-data'>
<table>
    <tr>
        <td></td>
        <td rowspan="3" style="width: 15px;"></td>
        <td></td>
    </tr>
    <tr>
        <td>Белков<br>
            <? echo $prot." из 200"?>
        </td>
        <td>Калорий<br>
            <? echo $cals." из 1925"?>
        </td>
    </tr>
    <tr>
        <td>
            <div class="prot">
                <div class="proc"></div>
                <div class="no-proc" style="height: <?=floor($no_proc)?>px;"></div>
                <div class="border_prot"><div class="test"><?echo $proc."%"?></div></div>
            </div>
        </td>
        <td>
            <div class="cals">
            <div class="calsc"></div>
            <div class="no-cals" style="height: <?=floor($no_cals)?>px;"></div>
            <div class="border_cals"><div class="test"><?echo round($calsc, 1)."%"?></div></div>
            </div>
        </td>
    </tr>
<?php
if ($date == date('Y-m-d')){
    echo "<tr>
        <td style='text-align: center;'>
            <input class='input' type='number' min='0' name='add_prot'>
        </td>
        <td><input type='submit' value='Добавить'></td>
        <td><input class='input' type='number' min='0' name='add_cals'><br></td>
    </tr>";
}?>
</table>
</form>