<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title><?=$title_page?></title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
<section>
    <?=$content_page?>
</section>
<br>
<div class="other_date">
<input type="checkbox" id="hd-1" class="hide"/>
<label for="hd-1" >Другая дата</label>
<div>
    <form method="POST" enctype="multipart/form-data">
        <input type="date" name="other_date" max="<?echo date('Y-m-d')?>" min="<?=$date_min?>"/>
        <input type="submit" value="ok">
    </form>
</div>
</div>
</body>